import React, { useState, useEffect } from "react";
import Counter from "../components/Counter/Index";

function CounterDoubleClickPage() {

    // Component did mount
    useEffect(() => {
        console.log("f-component: did mount");
    }, []);

    const [count, setCount] = useState(0);

    useEffect(() => {
        console.log("f-component: update");
        return function cleanup() {
            console.log("f-component: unmount");
        }
    }, [count]);

    function handleOnDoubleClick() {
        setCount(count + 1);
    }

    return (
        <div className="text-center">
            <Counter count={count} handleOnDoubleClick={handleOnDoubleClick} />
        </div>
    );
}

export default CounterDoubleClickPage;