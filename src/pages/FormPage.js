import React, { useState } from "react";
import Input from "../components/Form/Input";

function FormPage() {

    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [age, setAge] = useState('');

    function handleNameChange(e) {
        e.preventDefault();
        setName(e.target.value);
    }

    function handleEmailChange(e) {
        e.preventDefault();
        setEmail(e.target.value);
    }

    function handleAgeChange(e) {
        e.preventDefault();
        let age = e.target.value;
        if (age >= 0 && age <= 150) {
            setAge(age);
        }
    }

    function renderName(name) {
        if (name && name.length > 3) {
            return (
                <h4>Hello, my name : {name}</h4>
            );
        } else {
            return <></>;
        }
    }

    function renderEmail(email) {
        if (email && /^[\w-.]+@([\w-]+\.)+[\w-]{2,4}$/.test(email)) {
            return (
                <h4>and my email address : {email}</h4>
            );
        } else {
            return <></>;
        }
    }

    function renderAge(age) {
        if (age && age > 0) {
            return (
                <h4>I'm {age} years old</h4>
            );
        }
    }

    return (
        <div className="text-center">
            <Input 
                name="name"
                value={name}
                handleOnChange={handleNameChange}
                placeholder="Input your name"
            />

            <Input 
                name="email"
                value={email}
                handleOnChange={handleEmailChange}
                placeholder="Input your email"
            />

            <Input
                name="age"
                value={age}
                handleOnChange={handleAgeChange}
                placeholder="Input your age"
            />

            {renderName(name)}
            {renderEmail(email)}
            {renderAge(age)}
        </div>
    );
}

export default FormPage;