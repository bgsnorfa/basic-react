import React, { useEffect, useMemo, useState } from "react";

function WeatherPage() {

    const [lat, setLat] = useState(0.0);
    const [long, setLong] = useState(0.0);
    const [response, setResponse] = useState({});
    const [loading, setLoading] = useState(true);

    useMemo(() => {
        setLat(-6.1742321);
        setLong(106.8234136);
    }, []);

    // Component did mount
    useEffect(() => {
        fetchWeather();
        setLoading(false);
    }, []);

    // component update
    useEffect(() => {
        setLoading(true);
        fetchWeather();
        setLoading(false);
    }, [lat, long]);

    function fetchWeather() {
        fetch(`https://api.open-meteo.com/v1/forecast?latitude=${lat}&longitude=${long}&current_weather=true`)
            .then(resp => resp.json())
            .then(data => setResponse(data));
    }

    function handleLatChange(e) {
        setLat(e.target.value);
    }

    function handleLongChange(e) {
        setLong(e.target.value);
    }

    function renderLoading() {
        return <h3>Loading...</h3>;
    }

    function renderWeather() {
        return (
            <>
                <h3>Weather data for:</h3>
                <h4>Lat: {lat}</h4>
                <h4>Long: {long}</h4>
                <h4>Temperature: {response?.current_weather?.temperature}°C</h4>
                <h4>Wind speed: {response?.current_weather?.windspeed}</h4>
                <h4>Wind direction: {response?.current_weather?.winddirection}</h4>
            </>
        );
    }

    return (
        <div>
            {loading ? renderLoading() : renderWeather()}
            <input 
                className="form-control"
                type="number"
                name="lat"
                value={lat}
                onChange={handleLatChange}
                placeholder="Latitude"
            />

            <input 
                className="form-control"
                type="number"
                name="long"
                value={long}
                onChange={handleLongChange}
                placeholder="Longitude"
            />
        </div>
    );
}

export default WeatherPage;