import React from "react";
import ArticlesPage from "./pages/ArticlesPage";
import CounterDoubleClickPage from "./pages/CounterDoubleClickPage";
import CounterPage from "./pages/CounterPage";
import FormPage from "./pages/FormPage";
import WeatherPage from "./pages/WeatherPage";

function App() {

  return (
    <div className="container">
      <WeatherPage />
      <CounterPage />
      <hr />
      <CounterDoubleClickPage />
      <hr />
      <FormPage />
      <hr />
      <ArticlesPage />
    </div>
  );
}

export default App;
