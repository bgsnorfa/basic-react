import React from "react";
import Avatar from "./Avatar";

function Article({ person }) {
    return (
        <div className="card my-3 mx-3" style={{ width: '25rem' }}>
            <Avatar 
                src={person.img}
                alt={person.title}
            />
            <div className="card-body">
                <h5 className="card-title">{person.title}</h5>
                <h6 className="card-subtitle mb-2 text-muted">{person.role}</h6>
                <p className="card-text">{person.description}</p>
                <a href="#" className="card-link">Read more</a>
            </div>
        </div>
    );
}

export default Article;