import React from "react";

class Avatar extends React.Component {
    render() {

        const { src, alt } = this.props;

        return (
            <img
                className="card-img-top"
                src={src}
                alt={alt}
                width={286}
                height={180}
            />
        );
    }
}

// function Avatar({ src, alt }) {
//     return (
//         <img
//             className="card-img-top"
//             src={src}
//             alt={alt}
//             width={286}
//             height={180}
//         />
//     );
// }

export default Avatar;